global class SalesOrdersSchedule Implements Schedulable {
    
    public static final String SALES_ORDERS = 'SalesOrders';
    
    global void execute(SchedulableContext ctx)
    {
        Skie_Variable_Unleashed__c setting = Skie_Variable_Unleashed__c.getOrgDefaults();
        DateTime lastSync = setting.Sales_Orders_Last_Sync__c;
        
        setting.Sales_Orders_Last_Sync__c = System.now();
        update setting;
        
        SalesOrdersController controller = new SalesOrdersController(lastSync, SALES_ORDERS, 1);
        controller.enqueueCallout();
    }
}