@isTest
public class SalesOrdersControllerTest {

    public static String CRON_EXP = '0 0 * * * ?'; 
    
    @testSetup static void setup() 
    {
        Skie_Variable_Unleashed__c setting = new Skie_Variable_Unleashed__c();
        setting.Page_Size__c = 200;
        setting.Sales_Orders_Last_Sync__c = System.today();
        setting.Unleashed_API_Id__c = 'api-id';
        setting.Unleashed_API_Key__c = 'api-key';
        setting.Unleashed_Domain_Url__c = 'callout';
        insert setting;
        
        Account account = new Account();
        account.Name = 'test add';
        account.Guid__c = '43d7af78-73bd-44fa-9925-abc5624cd78a';
        insert account;
        
        Product2 product = new Product2();
        product.Name = 'Test Product';
        product.Guid__c = '38bb0521-62ea-441f-8afa-688a8e11f64c';
        insert product;
        
        Sales_Order__c order = new Sales_Order__c();
        order.Guid__c = '0d9009ca-d453-4add-a145-72c8ef485e55';
        insert order;
        
        Sales_Order_Line__c orderLine = new Sales_Order_Line__c();
        orderLine.Sales_Order__c = order.Id;
        orderLine.Guid__c = '402fdbf5-8d73-4216-8e60-a5fb62e1967a';
        insert orderLine;
    }
    
    @isTest static void testSyncSalesOrders_SuccessCase() {
        HttpResponse orderResponse = new HttpResponse();
        orderResponse.setStatusCode(200);
        orderResponse.setBody(UnleashedHttpCalloutMockTest.getSalesOrdersResponseForGET());
    
        Map<String, HttpResponse> expectedResponse = new Map<String, HttpResponse> {
            'callout/SalesOrders/1' => orderResponse
        };
        
        UnleashedHttpCalloutMockTest.setTestMockResponse(200, 'OK', null, expectedResponse);
        
        Test.startTest();
        
        System.schedule('Sync Sales Orders records created in Unleashed to SF', CRON_EXP, new SalesOrdersSchedule());
        
        Test.stopTest();
    }
    
         @isTest static void testSyncSalesOrdersWithTmpProductAndTmpAccount_SuccessCase() {
        HttpResponse orderResponse = new HttpResponse();
        orderResponse.setStatusCode(200);
        orderResponse.setBody(UnleashedHttpCalloutMockTest.getSalesOrdersWithTmpProductAndTmpAccountResponseForGET());
    
        Map<String, HttpResponse> expectedResponse = new Map<String, HttpResponse> {
            'callout/SalesOrders/1' => orderResponse
        };
        
        UnleashedHttpCalloutMockTest.setTestMockResponse(200, 'OK', null, expectedResponse);
        
        Test.startTest();
        
        System.schedule('Sync Sales Orders records created in Unleashed to SF', CRON_EXP, new SalesOrdersSchedule());
        
        Test.stopTest();
    }   
        
    @isTest static void testSyncSalesOrderByGuid() {
        HttpResponse orderResponse = new HttpResponse();
        orderResponse.setStatusCode(200);
        orderResponse.setBody(UnleashedHttpCalloutMockTest.getSalesOrderResponseForGET());
    
        Map<String, HttpResponse> expectedResponse = new Map<String, HttpResponse> {
            'callout/SalesOrders/xxx' => orderResponse
        };
        
        UnleashedHttpCalloutMockTest.setTestMockResponse(200, 'OK', null, expectedResponse);
        
        Test.startTest();
        
        SalesOrdersController.upsertRecordByGuid('SalesOrders', 'xxx');
        
        Test.stopTest();
    }
    
    @isTest static void testSyncSalesOrders_ErrorCase() {
        HttpResponse orderResponse = new HttpResponse();
        orderResponse.setStatusCode(500);
        orderResponse.setBody(UnleashedHttpCalloutMockTest.getErrorResponse());
    
        Map<String, HttpResponse> expectedResponse = new Map<String, HttpResponse> {
            'callout/SalesOrders/1' => orderResponse
        };
        
        UnleashedHttpCalloutMockTest.setTestMockResponse(500, 'Internal Server Error', null, expectedResponse);
        
        Test.startTest();
        
        System.schedule('Sync Sales Orders records created in Unleashed to SF', CRON_EXP, new SalesOrdersSchedule());
        
        Test.stopTest();
    }
    
}